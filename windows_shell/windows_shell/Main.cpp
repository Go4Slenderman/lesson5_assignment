#pragma once

#include <iostream>
#include <string>
#include <Windows.h>
#include "Helper.h"

#define EXIT "exit"
#define PATH_MAX 2048

using namespace std;

typedef unsigned int Uint;

vector<string> readCmd(); //Reads a command from the user and return it's parameters
string pwd(); //Returns the current working directory
void cd(string path); //Changes the current working directory
void create(string filename); //Creates a file with the given file name. If the file exists its overridden
void ls(); //Prints all the directories and file names of the current working directory
void runExe(string name); //Runs an executable file and prints its return code

void main()
{
	HMODULE handle = LoadLibrary("Secret.dll"); //Loading the dll
	if (handle == NULL) //If the library load has failed
	{
		cout << "Cannot load Secret.dll" << endl;
		system("pause");
		exit(0);
	}
	FARPROC funcPtr = GetProcAddress(handle, "TheAnswerToLifeTheUniverseAndEverything"); //Getting a pointer to the function from the dll
	if (funcPtr == NULL) //If there was an error getting the address of the function
	{
		cout << "Cannot retrieve the points to the function" << endl;
		system("pause");
		exit(0);
	}
	vector<string> cmd;
	cmd.push_back("");
	while (cmd[0] != EXIT)
	{
		cout << pwd() << ">"; //Printing the current working directory
		cmd = readCmd(); //Reading the user's command
		if (cmd.size() == 0) //If no command was entered
		{
			cmd.push_back("");
			continue;
		}
		else if (cmd[0] == "cd") //If the user wants to change the current working directory
		{
			if (cmd.size() >= 2) //If the path exits in the parameters list
				cd(cmd[1]);
			else
				cout << "Path missing" << endl;
		}
		else if (cmd[0] == "create") //If the user wants to create a new file in the current working directory
		{
			if (cmd.size() >= 2) //If the path exits in the parameters list
				create(cmd[1]);
			else
				cout << "Filename missing" << endl;
		}
		else if (cmd[0] == "ls") //If the user wants to watch the content of the current working directory
			ls();
		else if (cmd[0] == "secret") //If the user wants to know the answer to life the universe and everything
			cout << "The answer to life, the universe and everything is: " << funcPtr() << endl;
		else if (cmd[0].find(".exe") != string::npos) //If the user wants to run an execuable file
			runExe(cmd[0]);
		else
			cout << "Unknown Command: " << cmd[0] << endl;
	}
	FreeLibrary(handle); //Freeing the dll
}

vector<string> readCmd()
{
	string cmd = "";
	getline(cin, cmd); //Reading the user's input
	Helper::trim(cmd); //Removing the un-needed whitespaces in the beginning and end of the command
	return Helper::get_words(cmd);
}

string pwd()
{
	TCHAR path[PATH_MAX] = { 0 };
	DWORD handle = GetCurrentDirectory(PATH_MAX, path); //Getting the path of the current working directory
	if (handle == 0) //Checking for errors
	{
		cout << "GetCurretDirectory Failure" << endl;
		system("pause");
		exit(0);
	}
	return path;
}

void cd(string path)
{
	DWORD handle = SetCurrentDirectory(path.c_str()); //Setting the current working directory to the user's input
	if (handle == 0) //Checking for errors
		cout << "Invalid Path" << endl;
}

void create(string filename)
{
	HANDLE handle = CreateFile(filename.c_str(), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	if (handle == INVALID_HANDLE_VALUE) //Checking for errors
	{
		cout << "File Creation Failure" << endl;
		system("pause");
		exit(0);
	}
}

void ls()
{
	WIN32_FIND_DATA search_data;

	memset(&search_data, 0, sizeof(WIN32_FIND_DATA));

	string tmp = pwd() + "/*";

	HANDLE handle = FindFirstFile(tmp.c_str(), &search_data); //Finding the first file in the current working directory

	while (handle != INVALID_HANDLE_VALUE) //If the found result is valid
	{
		if (strcmp(search_data.cFileName, ".") && strcmp(search_data.cFileName, "..")) //Avoiding the print of the unnessecary dots
			cout << search_data.cFileName << endl;

		if (FindNextFile(handle, &search_data) == FALSE) //If there are no more files to traverse through
			break;
	}

	FindClose(handle); //Closing the handle
}

void runExe(string name)
{
	/*
		WaitForSingleObject waits until a process finishes its execution and then continues to the next line of code.
		GetExitCodeExecute returns the exit code of a process.
	*/
	HINSTANCE handle = ShellExecute(NULL, NULL, name.c_str(), NULL, NULL, SW_SHOW); //Running the executable
	if ((int)handle <= 32) //If the function has failed
	{
		cout << "The executable has failed to run" << endl;
		system("pause");
		exit(0);
	}
	LPDWORD exitCode = NULL;
	if (WaitForSingleObject(handle, INFINITE) == WAIT_FAILED) //Waiting for the executable to finish its run
	{
		cout << "Wait for single object has failed" << endl;
		system("pause");
		exit(0);
	}
	GetExitCodeProcess(handle, exitCode); //Getting the exit code of the executable file
	cout << "The program has exited with the exit code of: " << exitCode << endl;
}
